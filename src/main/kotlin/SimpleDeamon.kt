import java.util.concurrent.TimeUnit

class SimpleDeamon : Runnable {
    override fun run() {
        var countDown = 4
        while (countDown-- > 0) {
            println("${countDown+1} секунд до завершения ${javaClass.simpleName} из ${Thread.currentThread().name}")
            TimeUnit.SECONDS.sleep(1)
        }

        println("${javaClass.simpleName} завершен из ${Thread.currentThread().name}")
    }
}