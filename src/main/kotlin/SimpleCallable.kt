import java.util.concurrent.Callable

class SimpleCallable : Callable<String> {

    override fun call(): String {
        return "Hello from ${Thread.currentThread().name}"
    }

}