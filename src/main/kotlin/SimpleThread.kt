class SimpleThread(val message: String) : Thread() {

    override fun run() {
        println("${javaClass.simpleName} started with message \"$message\" in ${Thread.currentThread().name}")
    }

}