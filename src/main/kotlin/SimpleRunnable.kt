class SimpleRunnable : Runnable {

    companion object {
        var count = 0
    }

    private val id = count++

    override fun run() {
        println("${this.javaClass.simpleName} started with id $id in ${Thread.currentThread().name}" )
    }


}