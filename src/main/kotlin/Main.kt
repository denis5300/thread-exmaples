import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit
import kotlin.concurrent.thread


class Main {

    companion object {
        @JvmStatic fun main(args: Array<String>) {
            println("Thread Examples")

            val range = 0..1

            for (i in range)
                SimpleRunnable().run()

            for (i in range)
                Thread(SimpleRunnable()).start()

            for (i in range)
                SimpleThread("My Thread $i").start()

            object : Thread() {
                override fun run() {
                    println("Thread one created in ${Thread.currentThread().name}")
                }
            }.start()

            Thread {
                println("Thread two created in ${Thread.currentThread().name}")
            }.start()

            thread(start = true) {
                println("Thread three created in ${Thread.currentThread().name}")
            }

            val exec = Executors.newCachedThreadPool()
            for (i in range)
                exec.execute(SimpleRunnable())
            exec.shutdown()


            val execFixed = Executors.newFixedThreadPool(range.count())
            for (i in range)
                execFixed.execute(SimpleRunnable())
            execFixed.shutdown()

            val execSingle = Executors.newSingleThreadExecutor()
            for (i in range)
                execSingle.execute(SimpleRunnable())
            execSingle.shutdown()

            val execCall = Executors.newCachedThreadPool()
            val res = mutableListOf<Future<String>>()
            for (i in range)
                res.add(execCall.submit(SimpleCallable()))
            for (item in res) {
                println(item.get())
            }
            execCall.shutdown()

            Thread(SimpleDeamon()).apply {
                isDaemon = true
                start()
            }

            runBlocking {

                methodForKotlin()
                launch(CommonPool) {
                    methodForKotlin()
                }

            }

            TimeUnit.SECONDS.sleep(5)

        }

        private suspend fun methodForKotlin() {
            println("MethodForKotlin start from ${Thread.currentThread().name}")
        }

    }

}
